package com.astronaut.TrainingScheduler;

public class Room implements Cloneable {

    private String name;
    private boolean morningStatus,afternoonStatus;
    private Course morningCourse,afternoonCourse;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMorningStatus() {
        return morningStatus;
    }

    public void setMorningStatus(boolean morningStatus) {
        this.morningStatus = morningStatus;
    }

    public boolean isAfternoonStatus() {
        return afternoonStatus;
    }

    public void setAfternoonStatus(boolean afternoonStatus) {
        this.afternoonStatus = afternoonStatus;
    }

    public Course getMorningCourse() {
        return morningCourse;
    }

    public void setMorningCourse(Course morningCourse) {
        this.morningCourse = morningCourse;
    }

    public Course getAfternoonCourse() {
        return afternoonCourse;
    }

    public void setAfternoonCourse(Course afternoonCourse) {
        this.afternoonCourse = afternoonCourse;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
