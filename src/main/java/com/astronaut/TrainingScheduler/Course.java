package com.astronaut.TrainingScheduler;

public class Course {

	private int priority;
	private String name;
	private int time;
	private boolean matchStatus;

	public boolean isMatchStatus() {
		return matchStatus;
	}

	public void setMatchStatus(boolean matchStatus) {
		this.matchStatus = matchStatus;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}
}
