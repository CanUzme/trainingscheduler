package com.astronaut.TrainingScheduler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import static java.util.Collections.sort;

public class Main {

    private static ArrayList<Course> courseList = new ArrayList<Course>();
    private static ArrayList<Date> datesList = new ArrayList<Date>();

    public static void main(String[] args) {

        readCours();
        sortingCoursPriority();

        String roomInput1 = "roomAvailable.txt";
        String roomInput2 = "room available - real life.txt";

        readRoom(roomInput2);

        matchingRoom();

        showResult();
    }

    private static void showResult() {

        for (Date date : datesList) {
            System.out.print(date.getDate());

            if(!date.isMorningStatus()){
                System.out.print("\tMorning = ");
                if (date.getRoomList().get(0).getMorningCourse() != null) {
                    System.out.print(date.getRoomList().get(0).getMorningCourse().getName()+"\t@\t"+date.getRoomList().get(0).getName()+"\n");
                } else if (date.getRoomList().get(1).getMorningCourse() != null) {
                    System.out.print(date.getRoomList().get(1).getMorningCourse().getName()+"\t@\t"+date.getRoomList().get(1).getName()+"\n");
                }
            }
            if(!date.isAfternoonStatus()){
                System.out.print("\tAfternoon = ");
                if (date.getRoomList().get(0).getAfternoonCourse() != null) {
                    System.out.print(date.getRoomList().get(0).getAfternoonCourse().getName()+"\t@\t"+date.getRoomList().get(0).getName()+"\n");
                } else if (date.getRoomList().get(1).getAfternoonCourse() != null) {
                    System.out.print(date.getRoomList().get(1).getAfternoonCourse().getName()+"\t@\t"+date.getRoomList().get(1).getName()+"\n");
                }
            }
            System.out.println();
        }

        System.out.print("\n------------------------------------------------------------\n\n");

        for (Course course : courseList){
            if ( !course.isMatchStatus() ) {
                System.out.println(course.getName()+" can not arrange");
            }
        }
    }

    private static void matchingRoom() {


        for (int courseIndex = 0; courseIndex < courseList.size(); courseIndex++) {
            skipCourse:
            for (int dateIndex = 0; dateIndex < datesList.size(); dateIndex++) {
                skipDate:
                switch (courseList.get(courseIndex).getPriority()) {
                    case 0:

                        if (datesList.get(dateIndex).isMorningStatus() && courseList.get(courseIndex).getTime() < 4) {
                            if (datesList.get(dateIndex).getRoomList().get(0).isMorningStatus()) {
                                datesList.get(dateIndex).setMorningStatus(false);
                                datesList.get(dateIndex).getRoomList().get(0).setMorningCourse(courseList.get(courseIndex));
                                datesList.get(dateIndex).getRoomList().get(0).setMorningStatus(false);
                                courseList.get(courseIndex).setMatchStatus(true);
                                break skipCourse;

                            } else if (datesList.get(dateIndex).getRoomList().get(1).isMorningStatus()) {
                                datesList.get(dateIndex).setMorningStatus(false);
                                datesList.get(dateIndex).getRoomList().get(1).setMorningCourse(courseList.get(courseIndex));
                                datesList.get(dateIndex).getRoomList().get(1).setMorningStatus(false);
                                courseList.get(courseIndex).setMatchStatus(true);
                                break skipCourse;
                            }

                        }
                        if (datesList.get(dateIndex).isAfternoonStatus()) {
                            if (datesList.get(dateIndex).getRoomList().get(0).isAfternoonStatus()) {
                                datesList.get(dateIndex).setMorningStatus(false);
                                datesList.get(dateIndex).getRoomList().get(0).setAfternoonCourse(courseList.get(courseIndex));
                                datesList.get(dateIndex).getRoomList().get(0).setAfternoonStatus(false);
                                courseList.get(courseIndex).setMatchStatus(true);
                                break skipCourse;

                            } else if (datesList.get(dateIndex).getRoomList().get(1).isAfternoonStatus()) {
                                datesList.get(dateIndex).setMorningStatus(false);
                                datesList.get(dateIndex).getRoomList().get(1).setAfternoonCourse(courseList.get(courseIndex));
                                datesList.get(dateIndex).getRoomList().get(1).setAfternoonStatus(false);
                                courseList.get(courseIndex).setMatchStatus(true);
                                break skipCourse;
                            }
                        }

                        break skipDate;
                    case 1:

                        if (dateIndex + 1 < datesList.size()) {

                            if ((datesList.get(dateIndex).isMorningStatus() && datesList.get(dateIndex).isAfternoonStatus()) &&
                                    (datesList.get(dateIndex + 1).isMorningStatus() && datesList.get(dateIndex + 1).isAfternoonStatus()) &&
                                    (datesList.get(dateIndex).getRoomList().get(0).isMorningStatus() || datesList.get(dateIndex).getRoomList().get(1).isMorningStatus()) &&
                                    (datesList.get(dateIndex).getRoomList().get(0).isAfternoonStatus() || datesList.get(dateIndex).getRoomList().get(1).isAfternoonStatus()) &&
                                    (datesList.get(dateIndex + 1).getRoomList().get(0).isMorningStatus() || datesList.get(dateIndex + 1).getRoomList().get(1).isMorningStatus()) &&
                                    (datesList.get(dateIndex + 1).getRoomList().get(0).isAfternoonStatus() || datesList.get(dateIndex + 1).getRoomList().get(1).isAfternoonStatus())
                                    ) {

                                datesList.get(dateIndex).setMorningStatus(false);
                                datesList.get(dateIndex + 1).setMorningStatus(false);
                                datesList.get(dateIndex).setAfternoonStatus(false);
                                datesList.get(dateIndex + 1).setAfternoonStatus(false);

                                if (datesList.get(dateIndex).getRoomList().get(0).isMorningStatus()) {
                                    datesList.get(dateIndex).getRoomList().get(0).setMorningCourse(courseList.get(courseIndex));
                                } else {
                                    datesList.get(dateIndex).getRoomList().get(1).setMorningCourse(courseList.get(courseIndex));
                                }

                                if (datesList.get(dateIndex).getRoomList().get(0).isAfternoonStatus()) {
                                    datesList.get(dateIndex).getRoomList().get(0).setAfternoonCourse(courseList.get(courseIndex));
                                } else {
                                    datesList.get(dateIndex).getRoomList().get(1).setAfternoonCourse(courseList.get(courseIndex));
                                }

                                if (datesList.get(dateIndex + 1).getRoomList().get(0).isMorningStatus()) {
                                    datesList.get(dateIndex + 1).getRoomList().get(0).setMorningCourse(courseList.get(courseIndex));
                                } else {
                                    datesList.get(dateIndex + 1).getRoomList().get(1).setMorningCourse(courseList.get(courseIndex));
                                }

                                if (datesList.get(dateIndex + 1).getRoomList().get(0).isAfternoonStatus()) {
                                    datesList.get(dateIndex + 1).getRoomList().get(0).setAfternoonCourse(courseList.get(courseIndex));
                                } else {
                                    datesList.get(dateIndex + 1).getRoomList().get(1).setAfternoonCourse(courseList.get(courseIndex));
                                }

                                courseList.get(courseIndex).setMatchStatus(true);
                                break skipCourse;
                            }
                        }
                        break skipDate;
                    case 2:
                        if (datesList.get(dateIndex).isAfternoonStatus()) {
                            if (datesList.get(dateIndex).getRoomList().get(0).isAfternoonStatus()) {
                                datesList.get(dateIndex).setAfternoonStatus(false);
                                datesList.get(dateIndex).getRoomList().get(0).setAfternoonCourse(courseList.get(courseIndex));
                                courseList.get(courseIndex).setMatchStatus(true);
                                break skipCourse;
                            } else if (datesList.get(dateIndex).getRoomList().get(1).isAfternoonStatus()) {
                                datesList.get(dateIndex).setAfternoonStatus(false);
                                datesList.get(dateIndex).getRoomList().get(1).setAfternoonCourse(courseList.get(courseIndex));
                                courseList.get(courseIndex).setMatchStatus(true);
                                break skipCourse;
                            }
                        }

                        break skipDate;
                    case 3:

                        if (datesList.get(dateIndex).isMorningStatus()) {
                            if (datesList.get(dateIndex).getRoomList().get(0).isMorningStatus()) {
                                datesList.get(dateIndex).setMorningStatus(false);
                                datesList.get(dateIndex).getRoomList().get(0).setMorningCourse(courseList.get(courseIndex));
                                datesList.get(dateIndex).getRoomList().get(0).setMorningStatus(false);
                                courseList.get(courseIndex).setMatchStatus(true);
                                break skipCourse;

                            } else if (datesList.get(dateIndex).getRoomList().get(1).isMorningStatus()) {
                                datesList.get(dateIndex).setMorningStatus(false);
                                datesList.get(dateIndex).getRoomList().get(1).setMorningCourse(courseList.get(courseIndex));
                                datesList.get(dateIndex).getRoomList().get(1).setMorningStatus(false);
                                courseList.get(courseIndex).setMatchStatus(true);
                                break skipCourse;
                            }

                        }
                        if (datesList.get(dateIndex).isAfternoonStatus()) {
                            if (datesList.get(dateIndex).getRoomList().get(0).isAfternoonStatus()) {
                                datesList.get(dateIndex).setAfternoonStatus(false);
                                datesList.get(dateIndex).getRoomList().get(0).setAfternoonCourse(courseList.get(courseIndex));
                                datesList.get(dateIndex).getRoomList().get(0).setAfternoonStatus(false);
                                courseList.get(courseIndex).setMatchStatus(true);
                                break skipCourse;

                            } else if (datesList.get(dateIndex).getRoomList().get(1).isAfternoonStatus()) {
                                datesList.get(dateIndex).setAfternoonStatus(false);
                                datesList.get(dateIndex).getRoomList().get(1).setAfternoonCourse(courseList.get(courseIndex));
                                datesList.get(dateIndex).getRoomList().get(1).setAfternoonStatus(false);
                                courseList.get(courseIndex).setMatchStatus(true);
                                break skipCourse;
                            }
                        }

                        break skipDate;
                }

            }
        }
    }

    private static void sortingCoursPriority() {

        courseList.sort((o1, o2) -> o1.getPriority() - o2.getPriority());

    }

    private static void readCours() {

        BufferedReader br = null;
        double input = 0;
        try {
            String sCurrentLine;
            br = new BufferedReader(new FileReader("coursList.txt"));

            while ((sCurrentLine = br.readLine()) != null) {
                String[] message = sCurrentLine.split(",");

                Course cours = new Course();
                cours.setName(message[0]);

                String sTime[] = message[1].trim().split(" ");
                int time = Integer.valueOf(sTime[0]);
                cours.setTime(time);

                String keyword = "Foundation";
                boolean isFoundationCours = false;
                for (String word : cours.getName().trim().split(" ")) {
                    if (word.equalsIgnoreCase(keyword)) {
                        isFoundationCours = true;
                    }
                }

                if (isFoundationCours) {
                    cours.setPriority(0);
                    courseList.add(cours);
                } else {
                    switch (time) {
                        case 14:
                            cours.setPriority(1);
                            courseList.add(cours);
                            break;

                        case 4:
                            cours.setPriority(2);
                            courseList.add(cours);
                            break;

                        default:
                            cours.setPriority(3);
                            courseList.add(cours);
                            break;
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void readRoom(String roomInput) {
        BufferedReader br = null;
        double input = 0;
        try {
            String sCurrentLine;
            br = new BufferedReader(new FileReader(roomInput));

            int lastDate = 0;
            Date dateTemp = new Date();
            ArrayList<Room> roomList = new ArrayList<Room>();
            Room roomTemp1 = new Room();
            Room roomTemp2 = new Room();

            while ((sCurrentLine = br.readLine()) != null) {
                String[] message = sCurrentLine.split(",");

                if (message[1].equalsIgnoreCase("Orientation")) {
                    continue;
                } else {

                    String splitMess1[] = message[0].split(" ");
                    int date = Integer.valueOf(splitMess1[0].trim());

                    dateTemp.setDate(date);

                    if (splitMess1[1].trim().equalsIgnoreCase("Morning")) {

                        String[] splitMess2 = message[1].trim().split("=");

                        roomTemp1.setName(splitMess2[0]);
                        if (splitMess2[1].trim().equalsIgnoreCase("OK")) {
                            roomTemp1.setMorningStatus(true);
                        } else {
                            roomTemp1.setMorningStatus(false);
                        }

                        String[] splitMess3 = message[2].trim().split("=");

                        roomTemp2.setName(splitMess3[0]);
                        if (splitMess3[1].trim().equalsIgnoreCase("OK")) {
                            roomTemp2.setMorningStatus(true);
                        } else {
                            roomTemp2.setMorningStatus(false);
                        }


                    } else if (splitMess1[1].trim().equalsIgnoreCase("Afternoon")) {
                        lastDate = date;

                        String[] splitMess2 = message[1].trim().split("=");

                        roomTemp1.setName(splitMess2[0]);
                        if (splitMess2[1].trim().equalsIgnoreCase("OK")) {
                            roomTemp1.setAfternoonStatus(true);
                        } else {
                            roomTemp1.setAfternoonStatus(false);
                        }

                        String[] splitMess3 = message[2].trim().split("=");
                        roomTemp2.setName(splitMess3[0]);
                        if (splitMess3[1].trim().equalsIgnoreCase("OK")) {
                            roomTemp2.setAfternoonStatus(true);
                        } else {
                            roomTemp2.setAfternoonStatus(false);
                        }

                        roomList.add((Room) roomTemp1.clone());
                        roomList.add((Room) roomTemp2.clone());
                        dateTemp.setRoomList(roomList);

                        datesList.add((Date) dateTemp.clone());

                        roomList.clear();
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

}
