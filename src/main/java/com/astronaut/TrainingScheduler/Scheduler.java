package com.astronaut.TrainingScheduler;

public class Scheduler {
    private boolean canArrange = false;
    private Room room;
    private Course cours;

    public boolean isCanArrange() {
        return canArrange;
    }

    public void setCanArrange(boolean canArrange) {
        this.canArrange = canArrange;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Course getCours() {
        return cours;
    }

    public void setCours(Course cours) {
        this.cours = cours;
    }
}
