package com.astronaut.TrainingScheduler;

import java.util.ArrayList;

public class Date implements Cloneable {

    private int date;
    private ArrayList<Room> roomList = new ArrayList<Room>();
    private boolean morningStatus=true,afternoonStatus=true;

    public boolean isMorningStatus() {
        return morningStatus;
    }

    public void setMorningStatus(boolean morningStatus) {
        this.morningStatus = morningStatus;
    }

    public boolean isAfternoonStatus() {
        return afternoonStatus;
    }

    public void setAfternoonStatus(boolean afternoonStatus) {
        this.afternoonStatus = afternoonStatus;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public ArrayList<Room> getRoomList() {
        return roomList;
    }

    public void setRoomList(ArrayList<Room> roomList) {
        this.roomList = new ArrayList<Room>(roomList);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
